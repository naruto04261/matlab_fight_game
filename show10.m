function show10(obj,eventdata)
    global imscene4 scene scenetemp timestart rolech teamch s10ch s78ch s10_1 s10_2 s10_3 s10_4 s10_5 s10_6 gcf_norm p po_index1 po_index3 im choice flag choicetemp1 choicetemp mousemove rolenum c1 keyin c2 s10ch;
    global difficulty background difficult1 difficult3 normal1 normal3 easy1 easy3;
    global stage1_1 stage1_3 stage2_1 stage2_3 stage3_1 stage3_3 stage4_1 stage4_3 stage5_1 stage5_3;
    global pointax backh gamefirst s10ax s10diff s10stage; %game's timer
    
    for i=find(choicetemp1==1)
        while choice(i)==1
            choice(i)=unidrnd(rolenum);
        end
    end
    if mousemove~=1
        if (strcmp(keyin,c1{5}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{5}) && choicetemp(2)~=0)
            if s10ch==1
                scene=11;
                gamefirst=1;
                show11();
            elseif s10ch==2 || s10ch==6
                s78ch=0;
                difficulty=1;
                background=1;
                rolech=zeros(1,8);  %無玩家
                choice=zeros(1,8);  %皆不選擇角色
                choicetemp=zeros(1,8);  %皆不選擇角色
                choicetemp1=zeros(1,8);  %皆不選擇角色
                teamch=zeros(1,8);  %隊伍皆無
                timestart=0;
                if s10ch==6
                    flag=0;
                    scene=4;
                    clf;
                    axes('units','normalized','pos',[0 0 1 1]);
                    backh=image(imscene4);
                    axis off;
                    pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
                    pointax.Layer='top';
                    image(po_index1,'AlphaData',po_index3);
                    axis off;
                    drawnow;
                else
                    set(s10ax,'Cdata',[]);
                    set(s10diff,'Cdata',[]);
                    set(s10stage,'Cdata',[]);
                    scene=scenetemp;
                    show56();
                end
                s10ch=3;
            elseif s10ch==3
                for i=find(choicetemp1==1)
                    choice(i)=1;
                    while choice(i)==1
                        choice(i)=unidrnd(rolenum);
                    end
                end
            elseif s10ch==4
                background=background+1;
                if background==6
                    background=1;
                end
            elseif s10ch==5
                difficulty=difficulty+1;
                if difficulty==4
                    difficulty=1;
                end
            end
        elseif (strcmp(keyin,c1{1}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{1}) && choicetemp(2)~=0)
            s10ch=s10ch-1;
            if s10ch<1
                s10ch=6;
            end
        elseif (strcmp(keyin,c1{2}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{2}) && choicetemp(2)~=0)
            s10ch=s10ch+1;
            if s10ch>6
                s10ch=1;
            end
        end
        show56();
    else
        set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    end
    if scene==10
        switch s10ch
            case 1
                set(s10ax,'Cdata',s10_1);
            case 2
                set(s10ax,'Cdata',s10_2);
            case 3
                set(s10ax,'Cdata',s10_3);
            case 4
                set(s10ax,'Cdata',s10_4);
            case 5
                set(s10ax,'Cdata',s10_5);
            case 6
                set(s10ax,'Cdata',s10_6);
        end
        switch background
            case 1
                set(s10stage,'Cdata',stage1_1,'AlphaData',stage1_3);
            case 2
                set(s10stage,'Cdata',stage2_1,'AlphaData',stage2_3);
            case 3
                set(s10stage,'Cdata',stage3_1,'AlphaData',stage3_3);
            case 4
                set(s10stage,'Cdata',stage4_1,'AlphaData',stage4_3);
            case 5
                set(s10stage,'Cdata',stage5_1,'AlphaData',stage5_3);
        end
        switch difficulty
            case 1
                set(s10diff,'Cdata',easy1,'AlphaData',easy3);
            case 2
                set(s10diff,'Cdata',normal1,'AlphaData',normal3);
            case 3
                set(s10diff,'Cdata',difficult1,'AlphaData',difficult3);
        end
        set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    end
end