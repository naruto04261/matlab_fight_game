function [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,pic)
    global mousemove backh show1axes pointax gcf_norm p po_index1 po_index3 show1ax;
    if flag==0
        clf;
        c1posx=zeros(2,5);
        c1posy=zeros(2,5);
        c2posx=zeros(2,5);
        c2posy=zeros(2,5);
        axes('units','normalized','pos',[0 0 1 1]);
        backh=imagesc(pic);
        axis([1 1920 1 1357]);
        axis off;
        show1ax=cell(1,10);
        show1axes=cell(1,10);
    end
    if mousemove~=1 || flag==0
        for i=1:5
             [c1posx(:,i),c1posy(:,i),show1ax,show1axes]=showpic1(show1ax,show1axes,flag,i,0.3405,0.5381-(i-1)*0.11,string(c1{i}));
             [c2posx(:,i),c2posy(:,i),show1ax,show1axes]=showpic1(show1ax,show1axes,flag,i+5,0.7605,0.5381-(i-1)*0.11,string(c2{i}));
        end
    else
        mousemove=0;
    end
    if flag==0
        pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
        pointax.Layer='top';
        image(po_index1,'AlphaData',po_index3);
        axis off;
    else
        set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    end
    %drawnow;
    flag=1;
end
function [posx,posy,show1ax,show1axes]=showpic1(show1ax,show1axes,flag,num,x,y,pic)
try
    [img, ~, alphachannel] = imread("data\"+pic);
catch
    [img, ~, alphachannel] = imread("data\error");
end
    [wx,~,~]=size(img);
    posx=[x-0.05*wx/108;x+0.05*wx/108];
    posy=[y;y+0.1];
    if flag==0
        show1axes{1,num}=axes('units','normalized','pos',[posx(1,1) posy(1,1) 0.1*wx/108 0.1]);
        [szy,szx,~]=size(img);
        show1ax{1,num}=imagesc(img,'AlphaData',alphachannel);
        axis([1 szx 1 szy]);
        axis off;
    else
        [szy,szx,~]=size(img);
        set(show1axes{1,num},'xlim',[1 szx],'ylim',[1 szy]);
        set(show1ax{1,num},'Cdata',img,'AlphaData',alphachannel);
    end
end
