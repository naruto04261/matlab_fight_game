% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure wikth the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global keyflag;
global keyin;
global scene;
global c1;
global c2;
global c1press;
global c2press;
global flag backh pointax;
global c1posx;
global c1posy;
global c2posx;
global c2posy;
global imscene2;
global imscene3 imscene4;
global rolech choice rolenum timestart po_index1 po_index3 p gcf_norm;
global teamch t t_re keytemp mousemove keypressing nokey;
nokey=0;
keypressing=1;
mousemove=0;

switch(eventdata.Key)
    case 'rightarrow'
        keyin='right';
    case 'leftarrow'
        keyin='left';
    case 'uparrow'
        keyin='up';
    case 'downarrow'
        keyin='down';
    case 'return'
        keyin='enter';
    case 'control'
        keyin='ctrl';
    case 'comma'
        keyin=',';    
    case 'semicolon'
        keyin=';';   
    case 'leftbracket'
        keyin='[';
    case 'rightbracket'
        keyin=']';
    case 'numpad0'
        keyin='0';
    case 'numpad1'
        keyin='1';
    case 'numpad2'
        keyin='2';
    case 'numpad3'
        keyin='3';
    case 'numpad4'
        keyin='4';
    case 'numpad5'
        keyin='5';
    case 'numpad6'
        keyin='6';
    case 'numpad7'
        keyin='7';
    case 'numpad8'
        keyin='8';
    case 'numpad9'
        keyin='9';
    otherwise
        keyin=eventdata.Key;
end
if scene==2 
        if c1press(c1press==1)
            c1(c1press==1)={keyin};
            c1press(c1press==1)=0;
            [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
        end
        if c2press(c2press==1)
            c2(c2press==1)={keyin};
            c2press(c2press==1)=0;
            [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
        end
        keyin='';
end
if scene==3 
        if c1press(c1press==1)
            c1(c1press==1)={keyin};
            c1press(c1press==1)=0;
            [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
        end
        if c2press(c2press==1)
            c2(c2press==1)={keyin};
            c2press(c2press==1)=0;
            [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
        end
        keyin='';
end


if ~strcmp(keytemp,keyin) || keyflag==0
    keyflag=1;
    if scene==5 || scene==6
        if strcmp(c1{5},keyin)==1
            if rolech(1)==0
                rolech(1)=1; %可以選擇角色
                choice(1)=1; %1表示random
                timestart=0;
                stop(t);
                t_re=6;
            elseif rolech(1)==1
                rolech(1)=2; %鎖定角色
                teamch(1)=1;
            elseif rolech(1)==2
                rolech(1)=3; %鎖定team
            end
        end
        if strcmp(c2{5},keyin)
            if rolech(2)==0
                rolech(2)=1; %可以選擇角色
                choice(2)=1; %1表示random
                timestart=0;
                stop(t);
                t_re=6;
            elseif rolech(2)==1
                rolech(2)=2; %鎖定角色
                teamch(2)=1;
            elseif rolech(2)==2
                rolech(2)=3; %鎖定team
            end
        end
        if strcmp(c1{6},keyin) 
            if timestart~=1
                rolech(1)=rolech(1)-1;
                if rolech(1)==0
                    choice(1)=0;
                elseif rolech(1)==1
                    teamch(1)=0;
                end
            elseif choice(1)~=0 
                t_re=t_re-1;
            end
        end
        if strcmp(c2{6},keyin) 
            if timestart~=1
                rolech(2)=rolech(2)-1;
                if rolech(2)==0
                    choice(2)=0;
                elseif rolech(2)==1
                    teamch(2)=0;
                end
            elseif choice(2)~=0 
                t_re=t_re-1;
            end
        end
        if rolech(1)==1
            if strcmp(c1{3},keyin)
                choice(1)=choice(1)-1; %上一個角色
                if choice(1)<1
                    choice(1)=rolenum;
                end
            elseif strcmp(c1{4},keyin)
                choice(1)=choice(1)+1; %下一個角色
                if choice(1)>rolenum
                    choice(1)=1;
                end
            end
        end
        if rolech(2)==1
            if strcmp(c2{3},keyin)
                choice(2)=choice(2)-1; %上一個角色
                if choice(2)<1
                    choice(2)=rolenum;
                end
            elseif strcmp(c2{4},keyin)==1
                choice(2)=choice(2)+1; %下一個角色
                if choice(2)>rolenum
                    choice(2)=1;
                end
            end
        end
        if rolech(1)==2
            if strcmp(c1{3},keyin)==1
                teamch(1)=teamch(1)-1; %上一個team
                if teamch(1)<1
                    teamch(1)=5;
                end
            elseif strcmp(c1{4},keyin)
                teamch(1)=teamch(1)+1; %下一個角色
                if teamch(1)>5
                    teamch(1)=1;
                end
            end
        end
        if rolech(2)==2
            if strcmp(c2{3},keyin)
                teamch(2)=teamch(2)-1; %上一個角色
                if teamch(2)<1
                    teamch(2)=5;
                end
            elseif strcmp(c2{4},keyin)
                teamch(2)=teamch(2)+1; %下一個角色
                if teamch(2)>5
                    teamch(2)=1;
                end
            end
        end
        if ((rolech(1)==3 && rolech(2)==0) || (rolech(2)==3 && rolech(1)==0) || (rolech(2)==3 && rolech(1)==3)) &&(strcmp(c2{5},keyin) || strcmp(c1{5},keyin))
            timestart=1; %開始倒數計時
            try
                start(t);
            catch
            end
        end
        if rolech(1)<0 || rolech(2)<0
            rolech=zeros(1,8);  %無玩家
            choice=zeros(1,8);  %皆不選擇角色
            teamch=zeros(1,8);  %隊伍皆無
            clf;
            scene=4;
            axes('units','normalized','pos',[0 0 1 1]);
            backh=imagesc(imscene4);
            axis off;
            pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
            pointax.Layer='top';
            image(po_index1,'AlphaData',po_index3);
            axis off;
        else
            show56();
        end
    elseif scene==7 || scene==8
        show78();
    elseif scene==9
        show9();
    elseif scene==10
        show10();
    end
    keytemp=keyin;
end
