function show56()
    global mousemove s10ax s10diff s10stage s78ax pointax imscene5 rolepicax rolenameax randax backh flag scene teamax im po_index1 po_index3 gcf_norm p;
    if scene~=4
        if flag==0
            rolepicax=cell(1,8);
            teamax=cell(1,8);
            rolenameax=cell(1,8);
            randax=cell(1,8);
            clf;
            axes('units','normalized','pos',[0 0 1 1]);
            backh=imagesc(imscene5);
            axis off;
            for i=1:8
                showit(i);
            end
            axes('units','normalized','pos',[0.2474 0.4 0.505208 0.198968]);
            s78ax=imagesc([]);
            axis off;
            axes('units','normalized','pos',[0 0.7052313 0.3177083 0.29476787]);
            s10ax=imagesc([]);
            axis off;
            axes('units','normalized','pos',[0.171875 0.8456595 0.1359375 0.0552689757]);
            s10stage=imagesc([]);
            axis off;
            axes('units','normalized','pos',[0.171875 0.8098747 0.1359375 0.0552689757]);
            s10diff=imagesc([]);
            axis off;
            pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
            pointax.Layer='top';
            image(po_index1,'AlphaData',po_index3);
            axis off;
            flag=1;
        elseif flag==1
            if scene~=11
                if mousemove~=1
                for i=1:8
                    showit(i);
                end
                else
                set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);    
                end
            end
        end
    end
end
function showit(i)
    showrolename(i);
    showteam(i);
    showrand(i);
    showrolepic(i);
end
function showrolepic(i)
    global flag randax rolepicax choice naruto boruto kakashi t_re pic0 pic1 pic2 pic3 pic4 pic5 t;
    switch i
        case 1
            x2=0.01927;
            y2=0.6720707;
        case 2
            x2=0.265625;
            y2=0.6720707;
        case 3
            x2=0.5135417;
            y2=0.6720707;
        case 4
            x2=0.7598958;
            y2=0.6720707;
        case 5
            x2=0.01927;
            y2=0.18201916;
        case 6
            x2=0.265625;
            y2=0.18201916;
        case 7
            x2=0.5135417;
            y2=0.18201916;
        case 8
            x2=0.7598958;
            y2=0.18201916;
    end
    temp=[];
    if flag==0
        axes('units','normalized','pos',[x2 y2 0.2213541667 0.2601326455]);
        rolepicax{1,i}=imagesc([]);
        axis([0 318 0 264]);
        axis off;
    else
        if choice(i)==2
            temp=boruto;
        elseif choice(i)==3
            temp=naruto;
        elseif choice(i)==4
            temp=kakashi;
        elseif choice(i)==0 && t_re~=6
            switch t_re
                case 5
                    temp=pic5;
                case 4
                    temp=pic4;
                case 3
                    temp=pic3;
                case 2
                    temp=pic2;
                case 1
                    temp=pic1;
                case 0
                    temp=pic0;
                    stop(t);
            end
        end
    end
    if ~isempty(temp)
        set(randax{1,i},'Cdata',[],'AlphaData',[]);
        set(rolepicax{1,i},'Cdata',temp);
    end
end
function showrand(i)
    global choice flag rolepicax randax rand1 rand3;
    switch i
        case 1
            x2=0.055;
            y2=0.65;
        case 2
            x2=0.3;
            y2=0.65;
        case 3
            x2=0.55;
            y2=0.65;
        case 4
            x2=0.795;
            y2=0.65;
        case 5
            x2=0.055;
            y2=0.16;
        case 6
            x2=0.3;
            y2=0.16;
        case 7
            x2=0.55;
            y2=0.16;
        case 8
            x2=0.795;
            y2=0.16;
    end
    if flag==0
        axes('units','normalized','pos',[x2 y2 0.1515625 0.109064]);
        randax{1,i}=imagesc([]);
        axis off;
    end
    if choice(i)==1
        set(rolepicax{1,i},'Cdata',[]);
        set(randax{1,i},'Cdata',rand1,'AlphaData',rand3);
    elseif choice(i)==0
        set(rolepicax{1,i},'Cdata',[]);
        set(randax{1,i},'Cdata',[],'AlphaData',[]);
    end
end
function showrolename(i)
    global rolenameax flag rand1 rand3 boru1 boru3 naru1 naru3 kaka1 kaka3 choice;
    switch i
        case 1
            x=0.1075;
            y=0.505; 
        case 2
            x=0.355;
            y=0.505;
        case 3
            x=0.6;
            y=0.505;
        case 4
            x=0.848;
            y=0.505;
        case 5
            x=0.1075;
            y=0.025;
        case 6
            x=0.355;
            y=0.025;
        case 7
            x=0.6;
            y=0.025;
        case 8
            x=0.848;
            y=0.025;
    end
    temp1=[];
    temp3=[];
    if choice(i)==1
        temp1=rand1;
        temp3=rand3;
    elseif choice(i)==2
        temp1=boru1;
        temp3=boru3;
    elseif choice(i)==3
        temp1=naru1;
        temp3=naru3;
    elseif choice(i)==4
        temp1=kaka1;
        temp3=kaka3;
    end
    if flag==0
        axes('units','normalized','pos',[x y 0.1515625 0.109064]);
        rolenameax{1,i}=imagesc([]);
        axis off;
    end
    if ~isempty(temp1)
        set(rolenameax{1,i},'Cdata',temp1,'AlphaData',temp3);
    else
        set(rolenameax{1,i},'Cdata',[],'AlphaData',[]);
    end
end
function showteam(i)
    global none none_3 team1 team1_3 team2 team2_3 team3 team3_3 team4 team4_3;
    global noneb noneb_3 team1b team1b_3 team2b team2b_3 team3b team3b_3 team4b team4b_3;
    global flag teamch rolech teamax join1 join3;
    switch i
        case 1
            x=0.1075;
            y=0.555; 
        case 2
            x=0.355;
            y=0.555;
        case 3
            x=0.6;
            y=0.555;
        case 4
            x=0.848;
            y=0.555;
        case 5
            x=0.1075;
            y=0.075;
        case 6
            x=0.355;
            y=0.075;
        case 7
            x=0.6;
            y=0.075;
        case 8
            x=0.848;
            y=0.075;
    end
    temp1=[];
    temp3=[];
    if rolech(i)==3 || rolech(i)==0 
        if teamch(i)==0
            temp1="join1";
            temp3="join3";
        elseif teamch(i)==1
            temp1="none";
            temp3="none_3";
        else
            temp1="team"+int2str(teamch(i)-1);
            temp3="team"+int2str(teamch(i)-1)+"_3";
        end
    else
        if rolech(i)==1
            set(teamax{1,i},'Cdata',[],'AlphaData',[]);
        elseif teamch(i)==1
            temp1="noneb";
            temp3="noneb_3";
        elseif teamch(i)~=0
            temp1="team"+int2str(teamch(i)-1)+"b";
            temp3="team"+int2str(teamch(i)-1)+"b_3";
        end
    end
    if flag==0
        axes('units','normalized','pos',[x y 0.1515625 0.109064]);
        teamax{1,i}=imagesc([]);
        axis off;
    end
    if ~isempty(temp1)
        temp1=char(temp1);
        temp3=char(temp3);
        temp1=eval(temp1);
        temp3=eval(temp3);
        set(teamax{1,i},'Cdata',temp1,'AlphaData',temp3);
    end
end
