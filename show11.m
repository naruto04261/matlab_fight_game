function show11(~,~)
    global teamch keytemp backh nokeypress flag keyin gamefirst keypressing ymin ymax xmax xmin b1_1 gamet keyin1 keyin2 background c1 c2 playerx playery choice difficulty; 
    persistent gover_h gameover1 gameover3 jdirect playerax playerh fallingr fallingrmap fallingl fallinglmap tempsz bloodper chakuraper attkup attkfirst statetemp attackmode breaking_run break_runr break_runrmap break_runl break_runlmap attkr attkrmap attkl attklmap defendr defendrmap defendl defendlmap state direct standingr standingrmap standingl standinglmap walkingr walkingl walkingrmap walkinglmap runningr runningl runningrmap...
        runninglmap jumpingr jumpingl jumpingrmap jumpinglmap count ws img0 img0_mirror img1 img1_mirror map0 map0_mirror map1 map1_mirror ...
        backpic;
    persistent die blueax bloodax red redmap blue bluemap border bordermap attk_countdown showpicmax run_countdown j_pos j_show nokey jumpcount run_updown;
    if gamefirst==1
        clf;
        [gameover1,~,gameover3]=imread('data/gameover.png');
        jdirect=zeros(1,8); %跳的方向:0代表原地 1代表向右 2向左 3向上 4向下
        playerax=cell(1,8);
        playerh=cell(1,8);
        chakuraper=zeros(1,8);
        bloodper=ones(1,8);
        set(gcf,'windowbuttonmotionfcn','');
        set(gcf,'keypressfcn','');
        setptr(gcf,'arrow');
        [border,~,bordermap]=imread('data/border.png');
        [blue,~,bluemap]=imread('data/blue.png');
        [red,~,redmap]=imread('data/red.png');
        statetemp=zeros(1,8);
        attkfirst=ones(1,8);
        die=zeros(1,8);
        breaking_run=zeros(1,8);
        attackmode=ones(1,8);
        attkup=zeros(1,8); %攻擊模式是否更換
        nokey=0;
        nokeypress=zeros(1,8);
        attk_countdown=zeros(1,8);
        run_countdown=zeros(1,8);
        showpicmax=ones(1,8);
        showpicmax=showpicmax*4;
        j_show=[1 2 3 3 3 3 2 1];
        j_pos=[0 0.2 0.5 0.7 1 0.7 0.5 0.2];
        fallingr=cell(8,2);
        fallingrmap=cell(8,2);
        fallingl=cell(8,2);
        fallinglmap=cell(8,2);
        jumpcount=ones(1,8);
        attkr=cell(8,5,3);
        attkrmap=cell(8,5,3);
        attkl=cell(8,5,3);
        attklmap=cell(8,5,3);
        break_runr=cell(8,2);
        break_runrmap=cell(8,2);
        break_runl=cell(8,2);
        break_runlmap=cell(8,2);
        defendr=cell(8,1);
        defendrmap=cell(8,1);
        defendl=cell(8,1);
        defendlmap=cell(8,1);
        standingr=cell(8,4);
        standingrmap=cell(8,4);
        standingl=cell(8,4);
        standinglmap=cell(8,4);
        walkingr=cell(8,4);
        walkingrmap=cell(8,4);
        walkingl=cell(8,4);
        walkinglmap=cell(8,4);
        runningr=cell(8,3);
        runningrmap=cell(8,3);
        runningl=cell(8,3);
        runninglmap=cell(8,3);
        jumpingr=cell(8,3);
        jumpingrmap=cell(8,3);
        jumpingl=cell(8,3);
        jumpinglmap=cell(8,3);
        count=0;
        ws=ones(1,8);
        keyin='';
        keyin1='';
        keyin2='';
        state=zeros(1,8);
        run_updown=zeros(1,8);
        if background==1
            backpic=imresize(b1_1(1:506,1:1334,:),[1357 1920]);
        end
        direct=zeros(1,8);
        axes('units','normalized','pos',[0 0 1 0.75]);                 
        backh=imagesc(backpic);
        axis off;
        for i=1:8
            if choice(i)~=0
                tempsz=i;
                bordershow(i,border,bordermap);
                bloodper(i)=1;
                chakuraper(i)=1;
                if playerx(i)>=20
                    direct(i)=2; %臉朝左
                else
                    direct(i)=1; %臉朝右
                end
                switch choice(i)
                    case 2
                        temp="boruto";
                    case 3
                        temp="naruto";
                    case 4
                        temp="kakashi";
                end
                [img0,~,map0]=imread("data/"+temp+"_0.bmp");%不能直接存三圍的圖片到四維矩陣，會出問題(變白色)
                [img0_mirror,~,map0_mirror]=imread("data/"+temp+"_0_mirror.bmp");
                [img1,~,map1]=imread("data/"+temp+"_1.bmp");
                [img1_mirror,~,map1_mirror]=imread("data/"+temp+"_1_mirror.bmp");
                defendr{i,1}=img0(321:399,1:79,1:3);
                defendrmap{i,1}=map0(321:399,1:79);
                defendl{i,1}=img0_mirror(321:399,722:800,1:3);
                defendlmap{i,1}=map0_mirror(321:399,722:800);
                for j=1:5
                    attkr{i,j,2}=img0(81:159,(80*j+161:80*j+239),1:3);
                    attkrmap{i,j,2}=map0(81:159,(80*j+161:80*j+239));
                    attkl{i,j,2}=img0_mirror(81:159,(562-80*j:640-80*j),1:3);
                    attklmap{i,j,2}=map0_mirror(81:159,(562-80*j:640-80*j));
                    attkr{i,j,3}=img0(241:319,(80*j+321:80*j+399),1:3);
                    attkrmap{i,j,3}=map0(241:319,(80*j+321:80*j+399));
                    attkl{i,j,3}=img0_mirror(241:319,(402-80*j:480-80*j),1:3);
                    attklmap{i,j,3}=map0_mirror(241:319,(402-80*j:480-80*j));
                    if j~=5
                        standingl{i,j}=img0_mirror(1:79,(802-80*j:880-80*j),1:3);
                        standinglmap{i,j}=map0_mirror(1:79,(802-80*j:880-80*j));
                        standingr{i,j}=img0(1:79,(80*j-79:80*j-1),1:3);
                        standingrmap{i,j}=map0(1:79,(80*j-79:80*j-1));
                        walkingl{i,j}=img0_mirror(1:79,(802-80*j-80*4:880-80*j-80*4),1:3);
                        walkinglmap{i,j}=map0_mirror(1:79,(802-80*j-80*4:880-80*j-80*4));
                        walkingr{i,j}=img0(1:79,(80*j-79+80*4:80*j-1+80*4),1:3);
                        walkingrmap{i,j}=map0(1:79,(80*j-79+80*4:80*j-1+80*4));
                    end
                    if j<4
                        attkr{i,j,1}=img0(81:159,(80*j-79:80*j-1),1:3);
                        attkrmap{i,j,1}=map0(81:159,(80*j-79:80*j-1));
                        attkl{i,j,1}=img0_mirror(81:159,(802-80*j:880-80*j),1:3);
                        attklmap{i,j,1}=map0_mirror(81:159,(802-80*j:880-80*j));
                        runningl{i,j}=img0_mirror(161:239,(802-80*j:880-80*j),1:3);
                        runninglmap{i,j}=map0_mirror(161:239,(802-80*j:880-80*j));
                        runningr{i,j}=img0(161:239,(80*j-79:80*j-1),1:3);
                        runningrmap{i,j}=map0(161:239,(80*j-79:80*j-1));
                        jumpingl{i,j}=img0_mirror(481:559,(802-80*j:880-80*j),1:3);
                        jumpinglmap{i,j}=map0_mirror(481:559,(802-80*j:880-80*j));
                        jumpingr{i,j}=img0(481:559,(80*j-79:80*j-1),1:3);
                        jumpingrmap{i,j}=map0(481:559,(80*j-79:80*j-1));
                    end
                    if j<3
                        break_runr{i,j}=img0(321:399,(80*j+561:80*j+639),1:3);
                        break_runrmap{i,j}=map0(321:399,(80*j+561:80*j+639));
                        break_runl{i,j}=img0_mirror(321:399,(162-80*j:240-80*j),1:3);
                        break_runlmap{i,j}=map0_mirror(321:399,(162-80*j:240-80*j));
                        fallingr{i,j}=img0(241:319,(80*j+161:80*j+239),1:3);
                        fallingrmap{i,j}=map0(241:319,(80*j+161:80*j+239));
                        fallingl{i,j}=img0_mirror(241:319,(562-80*j:640-80*j),1:3);
                        fallinglmap{i,j}=map0_mirror(241:319,(562-80*j:640-80*j));
                    end
                end
                playerax{1,i}=axes('units','normalized','pos',[playerx(i)/40 (playery(i)+j_pos(jumpcount(i)))/28.27 0.1 0.1]);
                if direct(i)==1
                    playerh{1,i}=imagesc(standingr{i,ws(i)},'AlphaData',standingrmap{i,ws(i)});
                else
                    playerh{1,i}=imagesc(standingl{i,ws(i)},'AlphaData',standinglmap{i,ws(i)});
                end
                axis([1 79 1 79]);
                axis off;
                axes('units','normalized','pos',[0.36 0.4 0.2778 0.1967]);
                gover_h=imagesc([]);
                axis([1 400 1 200]);
                axis off;
            else
                bloodper(i)=0;
                chakuraper(i)=0;
            end
        end
        flag=0;
        blueax=cell(1,tempsz);
        bloodax=cell(1,tempsz);
        for i=1:tempsz
            blueax=chakura(i,chakuraper(i),blue,bluemap,blueax,flag);
            bloodax=blood(i,bloodper(i),red,redmap,bloodax,flag);
        end
        flag=1;
        gamefirst=0;
        start(gamet);
    else
        if mod(count,12)==0
            for i=1:tempsz
                %if choice(i)~=0
                if bloodper(i)<=0 
                    bloodper(i)=0;
                    chakuraper(i)=0;
                    state(i)=-1;
                end
                bloodax=blood(i,bloodper(i),red,redmap,bloodax,1);
                blueax=chakura(i,chakuraper(i),blue,bluemap,blueax,1);
                if statetemp(i)~=state(i)
                    ws(i)=1;
                end
                if run_updown(i)==-1 %跑步時下一格
                    if playery(i)>ymin
                        playery(i) = playery(i)-0.5;
                    end
                    run_updown(i)=0;
                elseif run_updown(i)==1 %跑步時上一格
                    if playery(i)<ymax
                        playery(i) = playery(i)+0.5;
                    end
                    run_updown(i)=0;
                end
                set(playerax{1,i},'pos',[playerx(i)/40 (playery(i)+j_pos(jumpcount(i)))/28.27 0.1 0.1]);
                if state(i)==-1 %倒下
                    showpicmax(i)=2;
                    if ws(i)==2 && bloodper(i)~=0
                        state(i)=0;
                    elseif ws(i)==2
                        die(i)=1;
                    end
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',fallingr{i,ws(i)},'AlphaData',fallingrmap{i,ws(i)});
                    else
                        set(playerh{1,i},'Cdata',fallingl{i,ws(i)},'AlphaData',fallinglmap{i,ws(i)});
                    end
                elseif state(i)==0 % 站立
                    showpicmax(i)=4;
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',standingr{i,ws(i)},'AlphaData',standingrmap{i,ws(i)});
                    else
                        set(playerh{1,i},'Cdata',standingl{i,ws(i)},'AlphaData',standinglmap{i,ws(i)});
                    end
                elseif state(i)==1 %上
                    showpicmax(i)=4;
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',walkingr{i,ws(i)},'AlphaData',walkingrmap{i,ws(i)});
                    else
                        set(playerh{1,i},'Cdata',walkingl{i,ws(i)},'AlphaData',walkinglmap{i,ws(i)});
                    end
                    playery(i) = playery(i)+0.5;
                    if playery(i)>ymax
                        playery(i) = ymax;
                    end
                elseif state(i)==2 %下
                    showpicmax(i)=4;
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',walkingr{i,ws(i)},'AlphaData',walkingrmap{i,ws(i)});
                    else
                        set(playerh{1,i},'Cdata',walkingl{i,ws(i)},'AlphaData',walkinglmap{i,ws(i)});
                    end
                    playery(i) = playery(i)-0.5;
                    if playery(i)<ymin
                        playery(i) = ymin;
                    end
                elseif state(i)==3 %左
                    showpicmax(i)=4;
                    set(playerh{1,i},'Cdata',walkingl{i,ws(i)},'AlphaData',walkinglmap{i,ws(i)});
                    playerx(i) = playerx(i)-0.5;
                    if playerx(i)<xmin
                        playerx(i)=xmin;
                    end
                elseif state(i)==4 %右
                    showpicmax(i)=4;
                    set(playerh{1,i},'Cdata',walkingr{i,ws(i)},'AlphaData',walkingrmap{i,ws(i)}); 
                    playerx(i) = playerx(i)+0.5;
                    if playerx(i)>xmax
                        playerx(i)=xmax;
                    end
                elseif state(i)==5 %攻擊
                    if attackmode(i)==1
                        showpicmax(i)=3;
                    else
                        showpicmax(i)=5;
                    end
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',attkr{i,ws(i),attackmode(i)},'AlphaData',attkrmap{i,ws(i),attackmode(i)});
                        if ws(i)==showpicmax(i)
                            j=(abs(playery-playery(i))<0.125 & (playerx-playerx(i))>0 & (playerx-playerx(i))<1.5 & (teamch~=teamch(i) | teamch==1)); 
                            bloodper(state==7 & j)=bloodper(state==7 & j)-0.02;
                            bloodper(state~=7 & j & state~=-1)=bloodper(state~=7 & j & state~=-1)-0.07;
                            state(state~=7 & j)=-1;
                        end
                    else
                        set(playerh{1,i},'Cdata',attkl{i,ws(i),attackmode(i)},'AlphaData',attklmap{i,ws(i),attackmode(i)}); 
                        if ws(i)==showpicmax(i)
                            j=(abs(playery-playery(i))<0.125 & (playerx(i)-playerx)>0 & (playerx(i)-playerx)<1.5 & (teamch~=teamch(i) | teamch==1));
                            bloodper(state==7 & j)=bloodper(state==7 & j)-0.02;
                            bloodper(state~=7 & j & state~=-1)=bloodper(state~=7 & j & state~=-1)-0.07;
                            state(state~=7 & j)=-1;
                        end
                    end
                    
                elseif state(i)==6 %跳
                    ws(i)=j_show(jumpcount(i));
                    jumpcount(i)=jumpcount(i)+1;
                    if jumpcount(i)==9
                        jumpcount(i)=1;
                        state(i)=jdirect(i);
                    else
                        if jdirect(i)==1
                            playery(i)=playery(i)+0.125;
                        elseif jdirect(i)==2
                            playery(i)=playery(i)-0.125;
                        elseif jdirect(i)==3
                            playerx(i)=playerx(i)-0.125;
                        elseif jdirect(i)==4
                            playerx(i)=playerx(i)+0.125;
                        end
                        if playerx(i)>xmax
                            playerx(i)=xmax;
                        elseif playerx(i)<xmin
                            playerx(i)=xmin;
                        elseif playery(i)>ymax
                            playery(i)=ymax;
                        elseif playery(i)<ymin
                            playery(i)=ymin;
                        end
                    end
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',jumpingr{i,ws(i)},'AlphaData',jumpingrmap{i,ws(i)});
                    else
                        set(playerh{1,i},'Cdata',jumpingl{i,ws(i)},'AlphaData',jumpinglmap{i,ws(i)}); 
                    end
                elseif state(i)==7 %防
                    if direct(i)==1
                        set(playerh{1,i},'Cdata',defendr{i,1},'AlphaData',defendrmap{i,1});
                    else
                        set(playerh{1,i},'Cdata',defendl{i,1},'AlphaData',defendlmap{i,1}); 
                    end
                elseif state(i)==8 %跑步
                    showpicmax(i)=3;
                    if ws(i)>3
                        ws(i)=1;
                    end
                    if direct(i)==1
                        if breaking_run(i)>0
                            set(playerh{1,i},'Cdata',break_runr{i,3-breaking_run(i)},'AlphaData',break_runrmap{i,3-breaking_run(i)});
                            playerx(i)=playerx(i)-0.5;
                            breaking_run(i)=breaking_run(i)-1;
                            if breaking_run(i)==0
                                state(i)=4;
                            end
                        else
                            set(playerh{1,i},'Cdata',runningr{i,ws(i)},'AlphaData',runningrmap{i,ws(i)});
                        end
                        if playerx(i)<xmax-0.5
                            playerx(i) = playerx(i)+1;
                        elseif playerx(i)==xmax-0.5
                            playerx(i)=xmax;
                        end
                    else
                        if breaking_run(i)>0
                            set(playerh{1,i},'Cdata',break_runl{i,3-breaking_run(i)},'AlphaData',break_runlmap{i,3-breaking_run(i)});
                            breaking_run(i)=breaking_run(i)-1;
                            playerx(i)=playerx(i)+0.5;
                            if breaking_run(i)==0
                                state(i)=3;
                            end
                        else
                            set(playerh{1,i},'Cdata',runningl{i,ws(i)},'AlphaData',runninglmap{i,ws(i)});
                        end
                        if playerx(i)>xmin+0.5
                            playerx(i) = playerx(i)-1;
                        elseif playerx(i)==xmin+0.5
                            playerx(i)=xmin;
                        end
                    end
                end
                statetemp(i)=state(i);    
                if state(i)~=6 && die(i)==0
                    ws(i)=ws(i)+1;
                    if ws(i)>showpicmax(i)
                        ws(i)=1;
                        if attkup(i)>0
                            attkup(i)=attkup(i)-1;
                            attackmode(i)=attackmode(i)+1;
                            if direct(i)==1 %右
                                playerx(i)=playerx(i)+0.5;
                                if playerx(i)>xmax
                                    playerx(i)=xmax;
                                end
                            else
                                playerx(i)=playerx(i)-0.5;
                                if playerx(i)<xmin
                                    playerx(i)=xmin;
                                end
                            end
                            if attackmode(i)>3
                                attkfirst(i)=1;
                                attackmode(i)=1;
                                ws(i)=1;
                            end
                        end
                    end
                end
                %end
            end
            keypressing=0;
            %drawnow;
        %toc
        end
        [keyin,keyin1,keyin2,nokeypress]=getkeypressing(c1,c2,nokeypress);
        temp=(run_countdown>0);
        run_countdown(temp)=run_countdown(temp)-1;
        temp1=attk_countdown>0;
        attk_countdown(temp1)=attk_countdown(temp1)-1;
        attkfirst(attk_countdown==0)=1;
        if strcmp(keyin,'f4')
            flag=0;
            set(gcf,'Pointer', 'custom', 'PointerShapeCData', NaN(16,16));
            stop(gamet);
            set(gcf,'windowbuttonmotionfcn','figure1_WindowButtonMotionFcn');
            set(gcf,'keypressfcn',{@figure1_KeyPressFcn,3});
            keytemp='';
        end
        teamcount=0;
        teamtemp=0;
        for i=1:8
            if bloodper(i)>0 && (teamtemp~=teamch(i) || teamch(i)==1)
                teamcount=teamcount+1;
                teamtemp=teamch(i);
            end
        end
        if teamcount>1
            [state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,run_updown,breaking_run,nokeypress]=...
                keycheck(1,state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,nokeypress,...
                count,run_updown,breaking_run,c1,keyin1);
            [state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,run_updown,breaking_run,nokeypress]=...
                keycheck(2,state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,nokeypress,...
                count,run_updown,breaking_run,c2,keyin2);
        else
            state=zeros(1,8);
            set(gover_h,'Cdata',gameover1,'Alphadata',gameover3);
        end
        count=count+1;
    end
end
function [state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,run_updown,breaking_run,nokeypress]...
    =keycheck(i,state,direct,jdirect,attkfirst,attkup,attackmode,attk_countdown,run_countdown,nokeypress,...
        count,run_updown,breaking_run,cx,keyinx)
    global choicetemp;
    if (choicetemp(i)~=0) && state(i)~=-1
        if state(i)~=6 && run_countdown(i)==0 && attk_countdown(i)==0 && attkup(i)==0
            if strcmp(keyinx,cx{1})     %上
                if state(i)==8
                    run_updown(i)=1;
                else
                    state(i)=1;
                end
            elseif strcmp(keyinx,cx{2})   %下
                if state(i)==8
                    run_updown(i)=-1;
                else
                    state(i)=2;
                end
            elseif strcmp(keyinx,cx{3}) %左
                if ~(state(i)==8 && direct(i)==2)
                    if state(i)==8 && direct(i)==1 && breaking_run(i)==0
                        breaking_run(i)=2; %兩次更新畫面為煞車狀態
                    elseif state(i)==8 && direct(i)==1 && breaking_run(i)~=0
                        breaking_run(i)=1;
                    else
                        breaking_run(i)=0;
                        run_countdown(i)=20; %倒數160毫秒
                        state(i)=3;
                        direct(i)=2;
                    end
                    nokeypress(i)=0;
                end
            elseif strcmp(keyinx,cx{4})   %右
                if ~(state(i)==8 && direct(i)==1)
                    if state(i)==8 && direct(i)==2 && breaking_run(i)==0
                        breaking_run(i)=2; %兩次更新畫面為煞車狀態
                        run_updown(i)=0; %因為matlab有bug
                    elseif state(i)==8 && direct(i)==2 && breaking_run(i)~=0
                        breaking_run(i)=1;
                        run_updown(i)=0; %因為matlab有bug
                    else
                        run_countdown(i)=20; %倒數160毫秒
                        state(i)=4;
                        direct(i)=1;
                    end
                    nokeypress(i)=0;
                end
            elseif strcmp(keyinx,cx{5}) && attkfirst(i)==1    %攻擊
                state(i)=5;
                attkfirst(i)=0;
                attackmode(i)=1;
                nokeypress(i)=0;
                attk_countdown(i)=30; %240毫秒內要再按一下
            elseif strcmp(keyinx,cx{6})     %跳
                if state(i)<=4 && state(i)>=0
                    jdirect(i)=state(i);
                else
                    jdirect(i)=0;
                end
                state(i)=6;
            elseif strcmp(keyinx,cx{7})    %防禦
                state(i)=7;
            elseif mod(count,12)==0 && state(i)~=8
                state(i)=0;
            end
        elseif run_countdown(i)~=0 && state(i)~=6 && nokeypress(i)~=0
            if strcmp(keyinx,cx{3}) %左
                if direct(i)==2
                    state(i)=8;
                else
                    state(i)=3;
                end
                %nokeypress(i)=0;
                direct(i)=2;
                run_countdown(i)=0; %倒數歸0
            elseif strcmp(keyinx,cx{4})   %右
                if direct(i)==1
                    state(i)=8;
                else
                    state(i)=4;
                end
                %nokeypress(i)=0;
                direct(i)=1;
                run_countdown(i)=0; %倒數歸0
            end
        elseif attk_countdown(i)~=0 && state(i)~=6 && nokeypress(i)~=0 && strcmp(keyinx,cx{5})
            attkup(i)=attkup(i)+1;
            attk_countdown(i)=30; %240毫秒
            nokeypress(i)=0;
        else
            if state(i)==6 || state(i)==5
                if strcmp(keyinx,cx{3}) %左
                    direct(i)=2;
                elseif strcmp(keyinx,cx{4})   %右
                    direct(i)=1;
                elseif strcmp(keyinx,cx{7}) && state(i)~=6
                    attk_countdown(i)=0;
                    attkup(i)=0;
                    nokeypress(i)=0;
                    state(i)=7;
                end
            else
                if strcmp(keyinx,cx{6})     %跳
                    if state(i)<=4 && state(i)>=0
                        jdirect(i)=state(i);
                    else
                        jdirect(i)=0;
                    end
                    state(i)=6;
                end
            end
        end
    end
end

function bloodax=blood(i,percent,red,redmap,bloodax,flag)
    if flag==0
        axes('units','normalized','pos',[0.02083+mod(i-1,4)*0.25 0.82351+(i<=4)*0.11767 0.2083*percent 0.03685]);
        bloodax{1,i}=imagesc(red(:,1:round(400*percent),:),'AlphaData',redmap(:,1:round(400*percent)));
        axis([1 400 1 50]);
        axis off;
    else
        set(bloodax{1,i},'Cdata',red(:,1:round(400*percent),:),'AlphaData',redmap(:,1:round(400*percent)));
    end
end

function blueax=chakura(i,percent,blue,bluemap,blueax,flag)
    if flag==0
        axes('units','normalized','pos',[0.02083+mod(i-1,4)*0.25 0.7720+(i<=4)*0.11767 0.2083*percent 0.03685]);
        blueax{1,i}=imagesc(blue(:,1:round(400*percent),:),'AlphaData',bluemap(:,1:round(400*percent)));
        axis([1 400 1 50]);
        axis off;
    else
        set(blueax{1,i},'Cdata',blue(:,1:round(400*percent),:),'AlphaData',bluemap(:,1:round(400*percent)));
    end
end
function bordershow(i,border,bordermap)
    axes('units','normalized','pos',[0.02083+mod(i-1,4)*0.25 0.7720+(i<=4)*0.11767 0.2083 0.03685]); 
    image(border,'AlphaData',bordermap);
    axis off;
    axes('units','normalized','pos',[0.02083+mod(i-1,4)*0.25 0.82351+(i<=4)*0.11767 0.2083 0.03685]);
    image(border,'AlphaData',bordermap);
    axis off;
end