function show78()
    global s78ax choicetemp1 first rolech pointax scene s78ch keyin c1 c2 choice choicetemp s78max im gcf_norm p po_index1 po_index3;
    if (strcmp(keyin,c1{5}) && choice(1)~=0)|| (strcmp(keyin,c2{5}) && choice(2)~=0)
        if s78ch~=0
            scene=9;
            choicetemp=choice;
            rolech(find(choice==0,1))=1;
            choice(find(choice==0,1))=1;
            first=1;
            set(s78ax,'Cdata',[]);
            show9();
        else
            set(s78ax,'Cdata',[])
            scene=10;
            choicetemp=choice;
            choicetemp1=choice;
            show10();
        end
    else
        if (strcmp(keyin,c1{3}) && choice(1)~=0)|| (strcmp(keyin,c2{3}) && choice(2)~=0)
            s78ch=s78ch-1;
            if s78ch<(8-scene)
                s78ch=s78max;
            end
        elseif (strcmp(keyin,c1{4}) && choice(1)~=0)|| (strcmp(keyin,c2{4}) && choice(2)~=0)
            s78ch=s78ch+1;
            if s78ch>s78max
                s78ch=8-scene;
            end
        end
        if scene==7
            set(s78ax,'Cdata',imread("data/s7_"+int2str(s78ch)+".jpg"));
        else
            set(s78ax,'Cdata',imread("data/s8_"+int2str(s78ch)+".jpg"));
        end
        set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
        pointax.Layer='top';
    end
    keyin='';
end