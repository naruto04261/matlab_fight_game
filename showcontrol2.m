function [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,pic)
    global mousemove backh pointax gcf_norm p po_index1 po_index3 show2axes;
    persistent show2ax;
    if flag==0
        clf;
        axes('units','normalized','pos',[0 0 1 1]);
        backh=imagesc(pic);
        axis off;
        show2ax=cell(1,4);
        show2axes=cell(1,4);
        c1posx=zeros(2,7);
        c1posy=zeros(2,7);
        c2posx=zeros(2,7);
        c2posy=zeros(2,7);
    end
    if mousemove~=1 || flag==0
        for i=6:7
             [show2ax,show2axes,c1posx(:,i),c1posy(:,i)]=showpic2(show2ax,show2axes,flag,i-5,0.3405,0.5381-(i-6)*0.11,string(c1(i)));
             [show2ax,show2axes,c2posx(:,i),c2posy(:,i)]=showpic2(show2ax,show2axes,flag,i-3,0.7605,0.5381-(i-6)*0.11,string(c2(i)));
        end
    else
        mousemove=0;
    end
    if flag==0
        pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
        pointax.Layer='top';
        image(po_index1,'AlphaData',po_index3);
        axis off;
    else
        set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    end
    flag=1;    
end
function [show2ax,show2axes,posx,posy]=showpic2(show2ax,show2axes,flag,num,x,y,pic)
    try
        [img, ~, alphachannel] = imread("data\"+pic);
    catch
        [img, ~, alphachannel] = imread("data\error");
    end
    [wx,~,~]=size(img);
    posx=[x-0.05*wx/108;x+0.05*wx/108];
    posy=[y;y+0.1];
    if flag==0
        [szy,szx,~]=size(img);
        show2axes{1,num}=axes('units','normalized','pos',[posx(1,1) posy(1,1) 0.1*wx/108 0.1]);
        show2ax{1,num}=imagesc(img,'AlphaData',alphachannel);
        axis([0 szx 0 szy]);
        axis off;
    else
        [szy,szx,~]=size(img);
        set(show2axes{1,num},'xlim',[0 szx],'ylim',[0 szy]);
        set(show2ax{1,num},'Cdata',img,'AlphaData',alphachannel);
    end
end