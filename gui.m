function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 05-May-2019 14:38:23

% Begin initialization code - DO NOT EDIT
opengl hardware;
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
%更改程式icon
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jFrame=get(gcf,'javaframe');
jicon=javax.swing.ImageIcon('data/p0.png');
jFrame.setFigureIcon(jicon);
handles.output = hObject;
%新增gui音樂,設置場景為1%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global scene;
global player;
scene=1;
[y,Fs] = audioread('s1.mp3');   %獲取音樂數據
player = audioplayer(y, Fs);
set(player,'StopFcn',{@audiostop,handles}); %音樂停止時呼叫函式，再次播放音樂

% 
%   for x = 1:10
%       disp(x)
%   end
% 
play(player);
global c1;
global c2;
global im;
global flag;
global c1press;
global c2press;
global keyflag;
global imscene1;
global imstart;
global imsetting;
global imexit;
global imok;
global imcancel;
global imnext;
global imscene2;
global imscene3;
global imscene4 im4_1 im4_2 im4_3;
global imscene5;
global imok_2;
global imcancel_2;
global imback;
global po_index1;
global po_index3;
global rolech choice rolenum boru1 boru3 join1 join3 kaka1 kaka3 naru1 naru3 rand1 rand3;
global naruto boruto kakashi;
global none none_3 team1 team1_3 team2 team2_3 team3 team3_3 team4 team4_3;
global noneb noneb_3 team1b team1b_3 team2b team2b_3 team3b team3b_3 team4b team4b_3;
global s10_1 s10_2 s10_3 s10_4 s10_5 s10_6 teamch timestart pic1 pic2 pic3 pic4 pic5 t t_re keytemp s78ch s78max s10ch;
global difficulty background difficult1 difficult3 normal1 normal3 easy1 easy3;
global stage1_1 stage1_3 stage2_1 stage2_3 stage3_1 stage3_3 stage4_1 stage4_3 stage5_1 stage5_3;
global b1_1 backh pointax p gcf_norm;
global xmax xmin ymax ymin playerx playery gamet;
xmax=37;
xmin=-2;
ymax=8.7;
ymin=3.7;
playerx=randi([0 (xmax-xmin)],1,8);
playerx=playerx-2;
playery=randi([0 round(ymax-ymin)],1,8);
playery=playery+3.7;
b1_1=imread('data/b1-1.bmp');
set(gcf,'doublebuffer','off');
[difficult1,~,difficult3]=imread('data\difficult.png');
[normal1,~,normal3]=imread('data\normal.png');
[easy1,~,easy3]=imread('data\easy.png');
[stage1_1,~,stage1_3]=imread('data\stage1.png');
[stage2_1,~,stage2_3]=imread('data\stage2.png');
[stage3_1,~,stage3_3]=imread('data\stage3.png');
[stage4_1,~,stage4_3]=imread('data\stage4.png');
[stage5_1,~,stage5_3]=imread('data\stage5.png');
difficulty=1;
background=1;
s10ch=3;
s10_1=imread("data/s10_1.jpg");
s10_2=imread("data/s10_2.jpg");
s10_3=imread("data/s10_3.jpg");
s10_4=imread("data/s10_4.jpg");
s10_5=imread("data/s10_5.jpg");
s10_6=imread("data/s10_6.jpg");
keytemp='';
s78ch=0;
s78max=7;
t_re=6;
t = timer('StartDelay', 0, 'Period', 1, 'TasksToExecute', 6, ...
          'ExecutionMode', 'fixedRate');
t.TimerFcn = {@tcall_fcn};
t.StopFcn = {@tstop_fcn};
gamet = timer('StartDelay', 0, 'Period', 0.008, 'TasksToExecute', inf, ...
          'ExecutionMode', 'fixedRate'); %0.008秒更新一次角色狀態
gamet.TimerFcn = {@show11};
gamet.StopFcn = {@gametstop};
timestart=0;
rolenum=4;
rolech=zeros(1,8);  %無玩家
choice=zeros(1,8);  %皆不選擇角色
teamch=zeros(1,8);  %隊伍皆無
pic1=imread('data\pic1.jpg');
pic2=imread('data\pic2.jpg');
pic3=imread('data\pic3.jpg');
pic4=imread('data\pic4.jpg');
pic5=imread('data\pic5.jpg');
imscene1=imread('data\p1.jpg');
imstart=imread('data\p2.jpg');
imsetting=imread('data\p3.jpg');
imexit=imread('data\p4.jpg');
naruto=imread('data\naruto.jpg');
boruto=imread('data\boruto.jpg');
kakashi=imread('data\kakashi.jpg');
[po_index1,~,po_index3]=imread('data\point.png');
[boru1,~,boru3]=imread('data\boru.png');
[naru1,~,naru3]=imread('data\naru.png');
[kaka1,~,kaka3]=imread('data\kaka.png');
[join1,~,join3]=imread('data\join.png');
[rand1,~,rand3]=imread('data\rand.png');
[none,~,none_3]=imread('data\none.png');
[noneb,~,noneb_3]=imread('data\none+.png');
[team1,~,team1_3]=imread('data\team1.png');
[team1b,~,team1b_3]=imread('data\team1+.png');
[team2,~,team2_3]=imread('data\team2.png');
[team2b,~,team2b_3]=imread('data\team2+.png');
[team3,~,team3_3]=imread('data\team3.png');
[team3b,~,team3b_3]=imread('data\team3+.png');
[team4,~,team4_3]=imread('data\team4.png');
[team4b,~,team4b_3]=imread('data\team4+.png');
imscene2=imread('data\pp1.jpg');
imok=imread('data\pp2.jpg');
imcancel=imread('data\pp3.jpg');
imnext=imread('data\pp4.jpg');
imscene3=imread('data\pp-2-1.jpg');
imok_2=imread('data\pp-2-3.jpg');
imcancel_2=imread('data\pp-2-4.jpg');
imback=imread('data\pp-2-2.jpg');
keyflag=0;
c1press=zeros(1,7);
c2press=zeros(1,7);
imscene4=imread('data\pp-3-1.jpg');
im4_1=imread('data\pp-3-2.jpg');
im4_2=imread('data\pp-3-3.jpg');
im4_3=imread('data\pp-3-4.jpg');
imscene5=imresize(imread('data\p5-1.jpg'),[1017,1440]);
im=[];
flag=1;

[c1,c2]=textread('control.txt','%s %s','headerlines',0);

%新增gui背景%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
axes('units','normalized','pos',[0 0 1 1]);
backh=imagesc(imscene1);
axis off;
p=get(gcf,'Position');
gcf_po = get(gcf, 'CurrentPoint');
gcf_norm=[gcf_po(1)/p(3) gcf_po(2)/p(4)];
pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
pointax.Layer='top';
image(po_index1,'AlphaData',po_index3);
axis off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(gcf, 'Pointer', 'custom', 'PointerShapeCData', NaN(16,16));
keysOfInterest=zeros(1,256);
KbQueueCreate(1,keysOfInterest)
KbQueueStart(1);
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
function gametstop(varargin)
    global scene gamefirst;
    %set(gcf,'WindowButtonMotionFcn',{@figure1_WindowButtonMotionFcn});
    %set(gcf, 'custom', 'PointerShapeCData', NaN(16,16));
    scene=10;
    gamefirst=1;
    show10();
    show10();
function tcall_fcn(obj,eventdata)
global t_re;
t_re=t_re-1;
show56();

% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global scene;
global c1;
global c2;
global c1posx;
global c1posy;
global c2posx;
global c2posy;
global flag;
global gcf_norm;
global c1press;
global c2press;
global backh pointax;
global imscene1;
global imscene2;
global imscene3;
global imscene4;
global po_index1 po_index3 p;
if scene==1
    if gcf_norm(1)>=0.446 && gcf_norm(1)<=0.542 && gcf_norm(2)>=0.619 && gcf_norm(2)<=0.67
        scene=4;
        set(backh,'Cdata',imscene4);
        pointax.Layer='top';
    elseif gcf_norm(1)>=0.43 && gcf_norm(1)<=0.564 && gcf_norm(2)>=0.44 && gcf_norm(2)<=0.5
        scene=2;
        flag=0;
        [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
    elseif gcf_norm(1)>=0.463 && gcf_norm(1)<=0.53 && gcf_norm(2)>=0.263 && gcf_norm(2)<=0.3
        flag=0;
        close;
    else
        flag=1;
    end
elseif scene==2 || scene==3
    if gcf_norm(1)>=0.3128 && gcf_norm(1)<=0.3578 && gcf_norm(2)>=0.0434 && gcf_norm(2)<=0.0929
        c=repmat({''}, 7, 1);
        for i=1:7
            c(i,1)={erase(string(c1(i)),"+")+' '+erase(string(c2(i)),"+")};
            c2(i)={erase(string(c2(i)),"+")};
            c2press(i)=0;
            c1(i)={erase(string(c1(i)),"+")};
            c1press(i)=0;
        end
        writecell(c,'control.txt');
        scene=1;
        clf;
        axes('units','normalized','pos',[0 0 1 1]);
        backh=imagesc(imscene1);
        axis off;
        pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
        pointax.Layer='top';
        image(po_index1,'AlphaData',po_index3);
        axis off;         
    elseif gcf_norm(1)>=0.7035 && gcf_norm(1)<=0.8171 && gcf_norm(2)>=0.0421 && gcf_norm(2)<=0.0979
        [c1,c2]=textread('control.txt','%s %s','headerlines',0);
        c1press=zeros(1,7);
        c2press=zeros(1,7);
        scene=1;
        clf;
        axes('units','normalized','pos',[0 0 1 1]);
        backh=imagesc(imscene1);
        axis off;
        pointax=axes('units','normalized','pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
        pointax.Layer='top';
        image(po_index1,'AlphaData',po_index3);
        axis off;    
    elseif gcf_norm(1)>=0.0191 && gcf_norm(1)<=0.1045 && gcf_norm(2)>=0.0347 && gcf_norm(2)<=0.0942 && scene==3
        scene=2;
        flag=0;
        for i=1:7
            c2(i)={erase(string(c2(i)),"+")};
            c2press(i)=0;
            c1(i)={erase(string(c1(i)),"+")};
            c1press(i)=0;
        end
        [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
    elseif gcf_norm(1)>=0.8915 && gcf_norm(1)<=0.9769 && gcf_norm(2)>=0.0434 && gcf_norm(2)<=0.0855 && scene==2
        scene=3;
        flag=0;
        for i=1:7
            c2(i)={erase(string(c2(i)),"+")};
            c2press(i)=0;
            c1(i)={erase(string(c1(i)),"+")};
            c1press(i)=0;
        end
        [c1posx,c1posy,c2posx,c2posy,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
    end
    if scene==2
        for i=1:5
            if gcf_norm(1)>=c1posx(1,i) && gcf_norm(1)<=c1posx(2,i) && gcf_norm(2)>=c1posy(1,i) && gcf_norm(2)<=c1posy(2,i)
                if ~contains(string(c1{i}),'+')
                    c1{i}=char(c1{i}+"+");
                    c1press(i)=1;
                end
                for j=1:5
                    if j~=i
                        if contains(string(c1{j}),'+')
                            c1(j)={erase(string(c1{j}),'+')};
                        end
                        c1press(j)=0;
                    end
                    if contains(string(c2{j}),'+')
                       c2(j)={erase(string(c2{j}),'+')};
                    end
                    c2press(j)=0;
                end
                [~,~,~,~,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
            elseif gcf_norm(1)>=c2posx(1,i) && gcf_norm(1)<=c2posx(2,i) && gcf_norm(2)>=c2posy(1,i) && gcf_norm(2)<=c2posy(2,i)
                if ~contains(string(c2{i}),'+')
                    c2{i}=char(c2{i}+"+");
                    c2press(i)=1;
                end
                for j=1:5
                    if j~=i
                        if contains(string(c2{j}),'+')
                            c2(j)={erase(string(c2{j}),'+')};
                        end
                        c2press(j)=0;
                    end
                    if contains(string(c1{j}),'+')
                       c1(j)={erase(string(c1{j}),'+')};
                    end
                    c1press(j)=0;
                end
                [~,~,~,~,flag]=showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
            end
        end
    elseif scene==3
        for i=6:7
            if gcf_norm(1)>=c1posx(1,i) && gcf_norm(1)<=c1posx(2,i) && gcf_norm(2)>=c1posy(1,i) && gcf_norm(2)<=c1posy(2,i)
                if ~contains(string(c1{i}),"+")
                    c1{i}=char(c1{i}+"+");
                    c1press(i)=1;
                end
                for j=6:7
                    if j~=i
                        if contains(string(c1{j}),'+')
                            c1(j)={erase(string(c1{j}),'+')};
                        end
                        c1press(j)=0;
                    end
                    if contains(string(c2{j}),'+')
                       c2(j)={erase(string(c2{j}),'+')};
                    end
                    c2press(j)=0;
                end
                [~,~,~,~,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
            elseif gcf_norm(1)>=c2posx(1,i) && gcf_norm(1)<=c2posx(2,i) && gcf_norm(2)>=c2posy(1,i) && gcf_norm(2)<=c2posy(2,i)
                if ~contains(string(c2{i}),'+')
                    c2{i}=char(c2{i}+"+");
                    c2press(i)=1;
                end
                for j=6:7
                    if j~=i
                        if contains(string(c2{j}),'+')
                            c2(j)={erase(string(c2{j}),'+')};
                        end
                        c2press(j)=0;
                    end
                    if contains(string(c1{j}),'+')
                       c1(j)={erase(string(c1{j}),'+')};
                    end
                    c1press(j)=0;
                end
                [~,~,~,~,flag]=showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
            end
        end
    end
elseif scene==4
    if gcf_norm(1)>=0.4633 && gcf_norm(1)<=0.5307 && gcf_norm(2)>=0.2553 && gcf_norm(2)<=0.2999
        scene=1;
        flag=1;
        set(backh,'Cdata',imscene1);
        pointax.Layer='top';
    elseif gcf_norm(1)>=0.4402 && gcf_norm(1)<=0.5508 && gcf_norm(2)>=0.5675 && gcf_norm(2)<=0.6183
        scene=5;
        flag=0;
        show56();
    elseif gcf_norm(1)>=0.4 && gcf_norm(1)<=0.593 && gcf_norm(2)>=0.4089 && gcf_norm(2)<=0.4622
        scene=6;
        flag=0;
        show56();
    else
        set(backh,'Cdata',imscene4);
        pointax.Layer='top';
    end
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global player gamet;
delete(hObject);
delete(eventdata);
set(player,'StopFcn','');
stop(player);  %關閉音樂
stop(gamet);
clear gamet;
clear player;
KbQueueStop; 

% --- Executes on key release with focus on figure1 and none of its controls.
function figure1_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)
global  scene keypressing nokey keyflag choicetemp c1 c2 keyin1 keyin2;
keyflag=0;
nokey=0;
keypressing=0;
% if scene==11
%     switch(eventdata.Key)
%         case 'rightarrow'
%             keyoff='right';
%         case 'leftarrow'
%             keyoff='left';
%         case 'uparrow'
%             keyoff='up';
%         case 'downarrow'
%             keyoff='down';
%         case 'return'
%             keyoff='enter';
%         case 'control'
%             keyoff='ctrl';
%         case 'comma'
%             keyoff=',';    
%         case 'semicolon'
%             keyoff=';';   
%         case 'leftbracket'
%             keyoff='[';
%         case 'rightbracket'
%             keyoff=']';
%         case 'numpad0'
%             keyoff='0';
%         case 'numpad1'
%             keyoff='1';
%         case 'numpad2'
%             keyoff='2';
%         case 'numpad3'
%             keyoff='3';
%         case 'numpad4'
%             keyoff='4';
%         case 'numpad5'
%             keyoff='5';
%         case 'numpad6'
%             keyoff='6';
%         case 'numpad7'
%             keyoff='7';
%         case 'numpad8'
%             keyoff='8';
%         case 'numpad9'
%             keyoff='9';
%         otherwise
%             keyoff=eventdata.Key;
%     end
%     if (choicetemp(1)~=0)
%         if find(strcmp(keyoff,c1))
%             nokeypress(1)=1;
% %             if strcmp(keyin1,keyoff)
% %                 %keyin1='';
% %                 nokeypress(1)=1;
% %             end
%         end
%     end
%     if (choicetemp(2)~=0)
% %         if find(strcmp(keyoff,c2))
% %             if strcmp(keyin2,keyoff)
% %                 keyin2='';
% %                 nokeypress(2)=1;
% %             end
% %         end
%     end
%end
function audiostop(hObject, eventdata, handles)
    global player;
    [y,Fs] = audioread('s1.mp3');   %獲取音樂數據
    player = audioplayer(y, Fs);
    set(player,'StopFcn',{@audiostop,handles});
    play(player);
