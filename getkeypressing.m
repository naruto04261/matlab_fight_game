function [key,keyin1,keyin2,nokeypress]=getkeypressing(c1,c2,nokeypress)
%keysOfInterest=zeros(1,256);
%KbQueueCreate(1,keysOfInterest)
%KbQueueStart(1);
%KbQueueStop; %最後面結束程式的時候
flag1=1;
flag2=1;
keyin1="";
keyin2="";
if KbMapKey(1,1)
    [~,~, keyCode] = KbCheck; %[keyIsDown,secs, keyCode] = KbCheck;
    [~,sz]=size(string(KbName(find(keyCode))));
    allkey=string(KbName(find(keyCode)));
    key= strings([1,sz]);
    for j=1:sz
        i=allkey(j);
        switch i
            case 'return'
                keyin='enter';
            case 'control'
                keyin='ctrl';
            case '1!'
                keyin='1';
            case '2@'
                keyin='2';
            case '3#'
                keyin='3';
            case '4$'
                keyin='4';
            case '5%'
                keyin='5';
            case '6^'
                keyin='6';
            case '7&'
                keyin='7';
            case '8*'
                keyin='8';
            case '9('
                keyin='9';
            case '0)'
                keyin='0';
            case ',<'
                keyin=',';
            case '.>'
                keyin='.';
            case '/?'
                keyin='/';
            otherwise
                if i~="right_shift" && i~="left_shift" && i~="right_control" && i~="left_control"
                    keyin=i;
                end
        end
        key(j)=keyin;
        for num=7:-1:1
            if strcmp(keyin,c1{num}) && flag1
                keyin1=keyin;
                if num>=5
                    flag1=0;
                end
            elseif strcmp(keyin,c2{num}) && flag2
                keyin2=keyin;
                if num>=5
                    flag2=0;
                end
            end
        end
    end
    if keyin1==""
        nokeypress(1)=1;
    end
    if keyin2==""
        nokeypress(2)=1;
    end
else
    key="";
    nokeypress(1)=1;
    nokeypress(2)=1;
end
end