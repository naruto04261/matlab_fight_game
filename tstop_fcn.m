function tstop_fcn(obj,eventdata)
global choice t_re scene s78ch pointax s78ax gcf_norm p scenetemp;
if t_re<=0
    t_re=6;
    count=0;
    for j=(choice~=0)
        count=count+j;
    end
    if scene==5 && count==1
        scenetemp=scene;
        scene=7;
        show56();
        s78ch=1;
        set(s78ax,'Cdata',imread("data/s7_"+int2str(s78ch)+".jpg"));
    else
        scenetemp=scene;
        scene=8;
        show56();
        s78ch=0;
        set(s78ax,'Cdata',imread("data/s8_"+int2str(s78ch)+".jpg"));
    end
    set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
else
    t_re=6;
end