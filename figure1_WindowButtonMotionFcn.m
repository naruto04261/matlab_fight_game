% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global scene;
global gcf_norm;
global p pointax mousemove;

if scene~=11
    mousemove=1;
    p=get(gcf,'Position');
    gcf_po = get(gcf, 'CurrentPoint');
    gcf_norm=[gcf_po(1)/p(3) gcf_po(2)/p(4)];
    show_moving();
    set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    drawnow;
end
