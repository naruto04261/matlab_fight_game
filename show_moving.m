function show_moving()
    persistent point;
    global backh mousemove scene;
    global imscene1;
    global imscene4 im4_1 im4_2 im4_3;
    global imstart;
    global imsetting;
    global imexit;
    global imok;
    global imcancel;
    global imnext;
    global imscene2;
    global imscene3;
    global imok_2;
    global imcancel_2;
    global imback;
    global c1;
    global c2;
    global c1posx;
    global c1posy;
    global c2posx;
    global c2posy;
    global gcf_norm flag ;
    mousemove=1;
    if gcf_norm(1)>=0.446 && gcf_norm(1)<=0.542 && gcf_norm(2)>=0.619 && gcf_norm(2)<=0.67 && scene==1
        show1(point,1,imstart);
        point=1;
    elseif gcf_norm(1)>=0.43 && gcf_norm(1)<=0.564 && gcf_norm(2)>=0.44 && gcf_norm(2)<=0.5 && scene==1
        show1(point,2,imsetting);
        point=2;
    elseif gcf_norm(1)>=0.463 && gcf_norm(1)<=0.53 && gcf_norm(2)>=0.263 && gcf_norm(2)<=0.3011 && scene==1
        show1(point,3,imexit);
        point=3;
    elseif gcf_norm(1)>=0.3128 && gcf_norm(1)<=0.3578 && gcf_norm(2)>=0.0434 && gcf_norm(2)<=0.0929 && scene==2
        show2(point,4,imok,gcf_norm);
        point=4;
    elseif gcf_norm(1)>=0.7035 && gcf_norm(1)<=0.8171 && gcf_norm(2)>=0.0421 && gcf_norm(2)<=0.0979 && scene==2
        show2(point,5,imcancel,gcf_norm);
        point=5;
    elseif gcf_norm(1)>=0.8915 && gcf_norm(1)<=0.9769 && gcf_norm(2)>=0.0434 && gcf_norm(2)<=0.0855 && scene==2
        show2(point,6,imnext,gcf_norm);
        point=6;
    elseif gcf_norm(1)>=0.0191 && gcf_norm(1)<=0.1045 && gcf_norm(2)>=0.0347 && gcf_norm(2)<=0.0942 && scene==3
        show2(point,7,imback,gcf_norm);
        point=7;
    elseif gcf_norm(1)>=0.3128 && gcf_norm(1)<=0.3578 && gcf_norm(2)>=0.0434 && gcf_norm(2)<=0.0929 && scene==3
        show2(point,8,imok_2,gcf_norm);
        point=8;  
    elseif gcf_norm(1)>=0.7035 && gcf_norm(1)<=0.8171 && gcf_norm(2)>=0.0421 && gcf_norm(2)<=0.0979 && scene==3
        show2(point,9,imcancel_2,gcf_norm);
        point=9;
    elseif gcf_norm(1)>=0.4402 && gcf_norm(1)<=0.5508 && gcf_norm(2)>=0.5675 && gcf_norm(2)<=0.6183 && scene==4
        show1(point,10,im4_1);
        point=10;
    elseif gcf_norm(1)>=0.4 && gcf_norm(1)<=0.593 && gcf_norm(2)>=0.4089 && gcf_norm(2)<=0.4622 && scene==4
        show1(point,11,im4_2);
        point=11;
    elseif gcf_norm(1)>=0.4633 && gcf_norm(1)<=0.5307 && gcf_norm(2)>=0.2553 && gcf_norm(2)<=0.2999 && scene==4
        show1(point,12,im4_3);
        point=12;
    else
        point=0;
        if scene==1
            set(backh,'Cdata',imscene1);
        elseif scene==2
            showcontrol1(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene2);
            set(backh,'Cdata',imscene2);
        elseif scene==3
            showcontrol2(c1,c2,c1posx,c1posy,c2posx,c2posy,flag,imscene3);
            set(backh,'Cdata',imscene3);
        elseif scene==4
            set(backh,'Cdata',imscene4);
        elseif scene==5 || scene==6
            show56();
        elseif scene==7 || scene==8
            show78();
        elseif scene==9
            show9();
        elseif scene==10
            show10();
        end
    end
end
function show1(point,id,im_move)
    global backh;
    if point~=id
        [y,Fs] = audioread('m_ok.wav');   %獲取音樂數據
        sound(y,Fs);        
    end
    set(backh,'Cdata',im_move)        
end
function show2(point,id,im_move,gcf_norm)
    global pointax backh;
    global p;
    if point~=id
        [y,Fs] = audioread('m_ok.wav');   %獲取音樂數據
        sound(y,Fs);
    end
    set(backh,'Cdata',im_move);
    set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    pointax.Layer='top';
end