function show9()
global t_re gcf_norm p pointax mousemove first s78ch scene scenetemp rolenum choice choicetemp choicetemp1 c1 keyin c2 rolech teamch;
if mousemove~=1
    count=0;
    for i=(choice~=choicetemp)
        if i==1
            count=count+1;
        end
    end
    if (strcmp(keyin,c1{5}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{5}) && choicetemp(2)~=0)
        if (find(rolech==1))
            if first~=1
                rolech(rolech==1)=2;
                teamch(rolech==2)=1;
            end
        elseif find(rolech==2)
            rolech(rolech==2)=3;
        elseif count<s78ch
            rolech(find(choice==0,1))=1;
            choice(find(choice==0,1))=1;
        end
    elseif (strcmp(keyin,c1{3}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{3}) && choicetemp(2)~=0)
        if find(rolech==1)
            choice(rolech==1)=choice(rolech==1)-1;
            if choice(rolech==1)<1
                choice(rolech==1)=rolenum;
            end
        elseif find(rolech==2)
            teamch(rolech==2)=teamch(rolech==2)-1;
            if teamch(rolech==2)<1
                teamch(rolech==2)=5;
            end
        end
    elseif (strcmp(keyin,c1{4}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{4}) && choicetemp(2)~=0)
        if find(rolech==1)
            choice(rolech==1)=choice(rolech==1)+1;
            if choice(rolech==1)>rolenum
                choice(rolech==1)=1;
            end
        elseif find(rolech==2)
            teamch(rolech==2)=teamch(rolech==2)+1;
            if teamch(rolech==2)>5
                teamch(rolech==2)=1;
            end
        end
    elseif (strcmp(keyin,c1{6}) && choicetemp(1)~=0)|| (strcmp(keyin,c2{6}) && choicetemp(2)~=0)
        if find(rolech==1)
            choice(rolech==1)=0;
            rolech(rolech==1)=0;
        elseif find(rolech==2)
            teamch(rolech==2)=0;
            rolech(rolech==2)=1;
        else
            temp=find(choice~=choicetemp);
            temp1=size(temp);
            try
                rolech(temp(temp1(2)))=2;
            catch
                rolech(temp)=2;
            end
        end
    end
    first=0;
    show56();
    if (size(find(choice))==size(find(choicetemp)))
        t_re=-1;
        scene=scenetemp;
        tstop_fcn();
        show78();
        choice=choicetemp;
    elseif count==s78ch
        if find(rolech==1)
        else
            if find(rolech==2)
            else
                scene=10;
                choicetemp1=choice;
                show10();
            end
        end
    end
    keyin='';
else
    set(pointax,'pos',[gcf_norm(1)-0.1/p(4) gcf_norm(2)-3.257/p(4) 12/p(3) 3.757/p(4)-0.1/p(4) ]);
    mousemove=0;
end
end